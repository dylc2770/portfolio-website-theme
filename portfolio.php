<?php /* Template Name: portfolio page */ ?>
<?php get_header(); ?>
<!-- PORTFOLIO -->
<div id="portfolioPage">
  <div class="container">
    <div id="projectsPage">
      <div class="row">
        <?php
          $catquery = new WP_Query('category_name=portfolio');
        	while ( $catquery->have_posts() ) {
        		$catquery->the_post();
            ?>
            <div class="item wow fadeInUp col-md-4 col-sm-6">
              <a href="<?php the_permalink(); ?>">
              <figure style="overflow: hidden" class="zoom-img">

              <img width="100%" src="<?php the_post_thumbnail_url(); ?>" alt="">
              
              </figure>
              <!-- <h4><?php echo get_the_title(); ?></h4> -->
              </a>
            </div>
            <?php
        	} // end while
?>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
