<?php /* Template Name: about page */ ?>
<?php get_header(); ?>

<div id="aboutPage">
 	<div class="container">
	  	<div class="aboutPage">

	  		<div class="meIntro">
		  		<div class="col-sm-12 col-md-3 col-md-offset-3">
			  		<img class="me" src="/wp-content/uploads/2017/07/me-2.png">
			  	</div>
			  	<div class="col-md-3">
					<div class="titleAbout">
						<h1 style="font-weight:400">Dylan Carver</h1>
						<h3 style="color:#4185C6; font-size: 15pt; font-weight:300">Designer / Developer</h3>
					</div>
			  	</div>
	  		</div>

	  	</div>

	  		<div class="col-md-8 col-md-offset-2">
		  		<div class="introText">
		  			<p>Hey I’m Dylan, 22 years old. I have a passion for design and development. I grew up in California and have lived here in Holland since I was 13. Since being here I have finished highschool and started my career into the media world. Startion in the video / animation sector and now schooled over to web development.</p>
		  		</div>
	  		</div>



	  		<div class="col-md-10 col-md-offset-1">
	  			<h1 class="wow fadeInUp">Experiences</h1>
				<hr class="wow fadeInUp">
	  			<div class="experience wow fadeInUp">
		  			<h3 class="title">Front end developer - 6 months</h3>
		  			<h3 class="year">2017</h3>
		  			<div style="clear: both"></div>
		  			<h4 class="location" style="color:#4185C6;">Alkmaar</h4>
	  			</div>

	  			<div class="experience wow fadeInUp">
		  			<h3 class="title">Front end developer (School Project) - 5 months</h3>
		  			<h3 class="year">2016</h3>
		  			<div style="clear: both"></div>
		  			<h4 class="location" style="color:#4185C6;">Amsterdam</h4>
	  			</div>


	  			<div class="experience wow fadeInUp">
		  			<h3 class="title">Motion Graphic Designer (Intern) - 7 months</h3>
		  			<h3 class="year">2014</h3>
		  			<div style="clear: both"></div>
		  			<h4 class="location" style="color:#4185C6;">Amsterdam</h4>
	  			</div>


	  			<h1 class="wow fadeInUp">Education</h1>
				<hr class="wow fadeInUp">
	  			<div class="education wow fadeInUp">
		  			<h3 class="title">Bachelor of Science - Web design & development</h3>
		  			<h3 class="year">2016</h3>
		  			<div style="clear: both"></div>
		  			<h4 class="location" style="color:#4185C6;">Amsterdam</h4>
	  			</div>



	  			<div class="education wow fadeInUp">
		  			<h3 class="title">Associate's degree - Web design & development</h3>
		  			<h3 class="year">2015</h3>
		  			<div style="clear: both"></div>
		  			<h4 class="location" style="color:#4185C6;">Amsterdam</h4>
	  			</div>


	  			<div class="education wow fadeInUp">
		  			<h3 class="title">First year Video / Audio / Animation</h3>
		  			<h3 class="year">2013</h3>
		  			<div style="clear: both"></div>
		  			<h4 class="location" style="color:#4185C6;">Amsterdam</h4>
	  			</div>

	  		</div>

		</div>
	</div>
</div>

<?php get_footer(); ?>
