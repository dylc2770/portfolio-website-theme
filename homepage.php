<?php /* Template Name: homepage */ ?>
<?php get_header(); ?>
<!-- BODY -->
<!-- HEADER -->
<div class="jumbotron vertical-center">
  <div class="container">
    <img src="wp-content/uploads/2017/07/logo-1.png" height="300px" alt="">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <p class="intro" style="font-weight:400">Hey I'm Dylan. As a designer and developer, I understand that websites should look good and work even better. <a id="portfolioAnchorJS" href="#portfolioAnchor">Here</a> are some projects I have worked on.
        </p>
      </div>
    </div>
  </div>
</div>
<!-- PORTFOLIO -->
<span class="portfolio" id="portfolioAnchor" ></span>
<div id="portfolio">
  <div class="container">
    <div id="projects">
      <div class="row">
        <?php
          $catquery = new WP_Query('category_name=homepage');
        	while ( $catquery->have_posts() ) {
        		$catquery->the_post();
            ?>
            <div class="item wow fadeInUp col-md-4 col-sm-6">
              <a href="<?php the_permalink(); ?>">
              <figure style="overflow: hidden" class="zoom-img">

              <img width="100%" src="<?php the_post_thumbnail_url(); ?>" alt="">
              
              </figure>
              <!-- <h4><?php echo get_the_title(); ?></h4> -->
              </a>
            </div>
            <?php
        	} // end while
?>
      </div>
    </div>
  </div>
</div>
<!-- CONTACT -->
<span class="contact" id="contactAnchor" ></span>
<div id="contact">

    <div class="container">
        <div class="row">
        <!-- CONTACT LEFT -->
      <div class="col-sm-4">
		<div class="contactText">
        <h1>Get in touch</h1>
        <h3 style="font-weight:300">If you're interested in potentially working together please don't hesitate to say hi!</h3>
		</div>
<div class="socialButtons">
		
      <a class="white" href="https://www.linkedin.com/in/dylan-carver" target="_blank"><i class="socialIcon fa fa-linkedin"></i></a>
      <a class="white" href="mailto:dylancarv@gmail.com"><i class="socialIcon fa fa-envelope "></i></a>
		</div>


      </div>
      <div class="col-sm-8">
      <!-- CONTACT RIGHT (FORM) -->
        <?php dynamic_sidebar( 'contact_homepage' ); ?>

      </div>
    </div>
  </div>
</div>

<?php get_footer('homepage'); ?>
