<div id="footer1">
  <div class="container text-center">
<a class="white" href="https://www.linkedin.com/in/dylan-carver" target="_blank"><i class="fa fa-linkedin"></i></a>
<a class="white" href="mailto:dylancarv@gmail.com"><i class="fa fa-envelope"></i></a>
<a href="<?php echo home_url(); ?>"><img width="35px" src="/wp-content/uploads/2017/07/logoWhite.png" /></a>
<p style="font-weight: 300">© 2017 dylancarver.nl | All rights reserved.</p>
</div>
</div>
<script>new WOW().init();</script>
<?php wp_footer(); ?> 
</body>
</html>
