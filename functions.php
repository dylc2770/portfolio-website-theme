<?php


function theme_name_scripts() {
wp_enqueue_style( 'bootstrap.min', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css');
wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
wp_enqueue_style( 'font-awesome.min', get_template_directory_uri() . '/font-awesome-4.7.0/css/font-awesome.min.css');
wp_enqueue_style( 'animate.min', get_template_directory_uri() . '/animate.min.css');

wp_enqueue_script( 'jquery-3.2.1.min', get_template_directory_uri() . '/jquery-3.2.1.min.js');
wp_enqueue_script( 'bootstrap.min', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js');
wp_enqueue_script( 'wow.min', get_template_directory_uri() . '/wow.min.js');
wp_enqueue_script( 'javascript', get_template_directory_uri() . '/javascript.js');

}

add_action('wp_enqueue_scripts', 'theme_name_scripts');






add_theme_support( 'custom-logo' );

add_theme_support( 'post-thumbnails' );

 /**
  * Register sidebars and widgetized areas.
  *
  */
 function arphabet_widgets_init() {

 	register_sidebar( array(
 		'name'          => 'contact_homepage',
 		'id'            => 'contact_home',
 		'before_widget' => '<div>',
 		'after_widget'  => '</div>'
 	) );
/*REGISTER HEADER MENU*/
 	function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
/*Initiate menu*/
add_action( 'init', 'register_my_menu' );

/*Initiate widgets*/
 }
 add_action( 'widgets_init', 'arphabet_widgets_init' );

 ?>
