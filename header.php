<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Dylan Carver</title>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <!-- <link rel="stylesheet" href="https://localhost:8888/foliosite/wp-content/themes/Portfolio/bootstrap/css/bootstrap.min.css"> -->
  <!-- <link rel="stylesheet" href="https://localhost:8888/foliosite/wp-content/themes/Portfolio/style.css"> -->
  <!-- <link rel="stylesheet" href="https://localhost:8888/foliosite/wp-content/themes/Portfolio/font-awesome-4.7.0/css/font-awesome.min.css"> -->
 <?php wp_head(); ?>
</head>
<body>
<!-- NAVBAR -->
<nav class="navbar navbar-fixed-top">
  <div class="container">
    <a href="<?php echo home_url(); ?>" class="navbar-brand"><img width="50px" src="/wp-content/uploads/2017/07/logo-1.png" alt=""></a>
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar" style="background-color:black;"></span>
        <span class="icon-bar" style="background-color:black;"></span>
        <span class="icon-bar" style="background-color:black;"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <div class="container">
        <?php wp_nav_menu( array( 'theme_location' => 'header-menu',
                                  'menu_class'=> 'nav navbar-nav navbar-right' ) ); ?>
      <!-- <ul class="nav navbar-nav navbar-right">

        <li><a href="<?php echo home_url(); ?>">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Portfolio</a></li>
        <li><a href="#">Workflow</a></li>
        <li><a id="contactAnchorJS" href="#contactAnchor">Contact</a></li>
      </ul> -->
      </div>
    </div>
  </div>
</nav>
