<?php get_header(); ?>
<?php
  // Start the Loop.
  while ( have_posts() ) : the_post();
   ?>
   <br>
   <br>
   <br>
   <br>

   <div class="container">
   <div class="col-sm-8 col-sm-offset-2">
   <h1><?php the_title(); ?></h1>
   <hr>
   <br>
    <p><?php the_content(); ?></p>
  </div>
</div>
  <?php endwhile;
?>
<?php get_footer(); ?>
